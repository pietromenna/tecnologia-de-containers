Tecnologia de containers: o que esta por tras?
FISL 2018
11 Jul 2018

Pietro Menna
https://pietro.menna.net.br
@pietromenna

* Sobre mim
- Engenheiro de software no SAP Labs Latin America no grupo de Big Data
- Ex-aluno do Recurse Center (spring 2 2015) em NYC
- Programo em Go
- Aberto para conversar sobre: Git, containers, recurse center, vagas na SAP para desenvolvedor no grupo de Big Data, programação em Go, e Kubernetes

* Sobre a esta apresentação

- Não iremos falar diretamente nem de Docker, nem de Rkt.
- É uma apresentação teorica.

* Agenda
- O que é um sistema operacional Linux?
- O que são os "containers"?
- cgroups e Namespaces?
- Rodando containers sem Docker ou Rkt

* O que é um sistema operacional Linux?

* ... o kernel, chamado Linux ...

.image imagem1.png _ 500

* ... as distribuçōes ...

.image logos-distros.jpg
* 

As distribuiçōes podem ser vistas como:

- ferramentas administrativas
- bibliotecas
- aplicativos
- diferentes layout para filesystem

A grosso modo, podemos resumir como "um monte de arquivos colocados de uma maneira determinada.


* O comando inicial (PID 1)

Anos atras tinha sentido este comentario, hoje so tem *systemd*.

Nem sempre foi assim: _Upstart_, e SysV.

* Definindo um sistema operacional Linux

- Usa o kernel Linux
- Sistema de arquivos especificos (distribuição)
- Comando inicial

* Disse que não ia falar de Docker, mas ...

.image imagem2.png


Encontramos isto: 


  FROM scratch
  ADD rootfs.tar.xz /
  CMD ["bash"]

* 
  FROM scratch
  ADD rootfs.tar.xz /

- Sistema de arquivos a ser utilizado.

* 
  CMD ["bash"]

- Comando inicial a ser executado.

* Mas, cade o kernel?

É o mesmo da maquina que esta rodando o container. 

Virtualização leve ou "lightweight".

* Maquinas virtuais

.image imagem4.png _ 500

* Docker ou containers

.image imagem3.png _ 500

* Então, o que são os "containers"

- Linux Kernel
- Tem um sistema de arquivos (imagem)
- Tem comando inicial (entrypoint)

* O que deve fazer uma maquina virtual?

- Não ser *real*.
- Deve executar e se comportar como se fosse uma maquina real.
- Se encontra num ambiente "isolado".
- Podemos limitar os recursos.

* ambiente "isolado"

* recurso de namespaces do kernel

  $ man 7 namespaces
  NAMESPACES(7)              Linux Programmer's Manual             NAMESPACES(7)
  
  NAME
         namespaces - overview of Linux namespaces
  
  DESCRIPTION
         A namespace wraps a global system resource in an abstraction that makes
         it appear to the processes within the namespace that  they  have  their
         own  isolated  instance  of the global resource.  Changes to the global
         resource are visible to other processes that are members of the  names‐
         pace,  but  are invisible to other processes.  One use of namespaces is
         to implement containers.
  
         Linux provides the following namespaces:
  
         Namespace   Constant          Isolates
         Cgroup      CLONE_NEWCGROUP   Cgroup root directory
         IPC         CLONE_NEWIPC      System V IPC, POSIX message queues
         Network     CLONE_NEWNET      Network devices, stacks, ports, etc.
         Mount       CLONE_NEWNS       Mount points
         PID         CLONE_NEWPID      Process IDs
         User        CLONE_NEWUSER     User and group IDs
         UTS         CLONE_NEWUTS      Hostname and NIS domain name

* 

Traduzindo:
  
  A funcionalidade de namespaces engloba um sistema global de recursos numa abstração
  que faz que para um processo dentro de um namespace ele tenha a sua própria instancia 
  isolada dos recursos globais. 

Em outras palavras: isola processos, usuarios, mounts, em _espaços_.

Algums espaços: pid, usuarios, mounts.

A ferramenta *unshare* permite criar processos com namespace não compartilhado com o "pai".

* 

*DEMO*:

*unshare* com com os parametros: 
--fork: o processo é executado como filho
--pid: não compartilhar o PID namespace 
--mount-proc:  remontar o /proc para o processo

  $ sudo unshare --fork --pid --mount-proc bash




  $ ps -ef
  UID         PID   PPID  C STIME TTY          TIME CMD
  root          1      0  0 21:59 pts/0    00:00:00 bash
  root         29      1  0 21:59 pts/0    00:00:00 ps -ef

* 

*Em*outro*terminal*paralelo*

  $ pstree
  $ ps -aux
  USER        PID %CPU %MEM    VSZ   RSS TTY      STAT START   TIME COMMAND
  ...
  root       3324  0.0  0.4 370216  8660 pts/0    S    21:59   0:00 sudo unshare --fork --pid --mount-proc bash
  root       3325  0.0  0.0 208932   616 pts/0    S    21:59   0:00 unshare --fork --pid --mount-proc bash
  root       3326  0.0  0.2 220208  4176 pts/0    S+   21:59   0:00 bash
  pfm        3380  0.0  0.2 220196  5116 pts/1    Ss   22:00   0:00 bash
  ...

*O*que*aprendemos*?

- Namespaces permite isolar recursos por em "espaços".

* limitando recursos

* recurso de cgroups do kernel

  $ man 7 cgroups
  CGROUPS(7)                Linux Programmer's Manual               CGROUPS(7)
  NAME         top
         cgroups - Linux control groups
  DESCRIPTION         top
         Control cgroups, usually referred to as cgroups, are a Linux kernel
         feature which allow processes to be organized into hierarchical
         groups whose usage of various types of resources can then be limited
         and monitored.  The kernel's cgroup interface is provided through a
         pseudo-filesystem called cgroupfs.  Grouping is implemented in the
         core cgroup kernel code, while resource tracking and limits are
         implemented in a set of per-resource-type subsystems (memory, CPU,
         and so on).

Traduzindo: funcionalidades criadas para organizar recursos de forma hierárquica 
com a finalidade que possam ser monitorados e limitados.

* Ferramentas de cgroups

- *cgcreate*: permite criar um novo grupo de controle.
- *cgclassify*: permite colocar processos embaixo de um grupo de controle.

* Não tem demo...

* Containers sem Docker

- Docker foi construido baseado em _lxc_. 
- Uma excellente fonte de conhecimento sobre containers: _man_lxc_
- A ferramenta *systemd-nspawn*

* DEMO

* Conclusão

